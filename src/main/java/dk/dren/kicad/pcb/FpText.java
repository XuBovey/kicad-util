package dk.dren.kicad.pcb;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Delegate;

@AllArgsConstructor
@Getter
public class FpText implements Node {
    public static final String NAME = "fp_text";

    public static final String REFERENCE = "reference";
    public static final String VALUE = "value";
    public static final String USER = "user";

    @Delegate
    private RawNode rawNode;

    public String getTextKind() {
        return getAttributes().get(0).getValue();
    }

    public String getTextValue() {
        return getAttributes().get(1).getValue();
    }

    @Override
    public String toString() {
        return getTextKind()+"="+getTextValue();
    }

    /**
     * Clones the style of the template text to this one, that is everything
     * is copied from the template, except for the text itself, which is retained
     */
    public void cloneStyle(FpText template) {
        FpText newFpText = KicadPcbSerializer.clone(template);

        newFpText.getAttributes().get(1).setValue(getAttributes().get(1));

        rawNode = newFpText.getRawNode();
    }

    public Position getPosition() {
        return (Position)getProperty(Position.AT);
    }
}
