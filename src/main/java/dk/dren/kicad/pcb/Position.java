package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import dk.dren.kicad.primitive.XYR;
import lombok.Getter;
import lombok.Setter;
import org.decimal4j.immutable.Decimal6f;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A position, with an x and an y coordinate in mm and an optional rotation in degrees
 */
@Getter
public class Position implements Node, Comparable<Position> {
    public static final String AT = "at";
    public static final String XY = "xy";
    public static final String START = "start";
    public static final String END = "end";

    private final String name;

    public Position(String name, Point a) {
        this.name = name;
        xyr = new XYR(a);
    }

    public static boolean isAtNode(RawNode rawNode) {
        if (!(rawNode.getName().equals(AT) || rawNode.getName().equals(XY) || rawNode.getName().equals(START) || rawNode.getName().equals(END))) {
            return false;
        }

        List<MutableValue> attributes = rawNode.getAttributes();
        return (attributes.size() == 2 || attributes.size() == 3)
                && rawNode.getAllChildren().size() == attributes.size();
    }

    @Setter
    private XYR xyr;

    public static Point ofVia(RawNode n) {
        return ((Position) n.getProperty(Position.AT)).withoutRotation();
    }

    public Point withoutRotation() {
        return xyr.withoutRotation();
    }

    public Position(RawNode rawNode) {
        name = rawNode.getName();
        Decimal6f x = rawNode.getAttributes().get(0).getDecimal6f();
        Decimal6f y = rawNode.getAttributes().get(1).getDecimal6f();
        Decimal6f rotation;
        if (rawNode.getAttributes().size() == 3) {
            rotation = rawNode.getAttributes().get(2).getDecimal6f();
        } else {
            rotation = Decimal6f.ZERO;
        }
        xyr = new XYR(x,y,rotation);
    }

    public String getName() {
        return name;
    }

    @Override
    public Collection<NodeOrValue> getAllChildren() {
        ArrayList<NodeOrValue> a = new ArrayList<>();
        a.add(new MutableValue(xyr.getX()));
        a.add(new MutableValue(xyr.getY()));
        if (!xyr.getRotation().isZero()) {
            a.add(new MutableValue(xyr.getRotation()));
        }
        return a;
    }

    @Override
    public String toString() {
        return name+"{" + xyr + '}';
    }

    @Override
    public int compareTo(Position o) {
        return xyr.compareTo(o.xyr);
    }
}
