package dk.dren.kicad.cmd;

import dk.dren.kicad.layoutclone.*;
import lombok.Getter;
import org.decimal4j.immutable.Decimal6f;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@Getter
@CommandLine.Command(name = "array-clone", description = "Clones a zone on the PCB to an array that's offset from the template by a certain x and y pitch.\n" +
        "\n" +
        "All parts must come from hierachial sub-sheets where the annotations are set to start from the next 100 or 1000 series."

)
public class ArrayCloneCmd implements  Callable<Integer> {

    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @CommandLine.Option(
            names = {"-r", "--ref", "--reference"},
            description = "The offsets from the template zone module references to each of the target groups of modules\nRepeat this for each of the groups that have to be placed",
            required = true
            )
    private CommaSeparatedIntegerList referenceOffsets;

    @CommandLine.Option(
            names = {"-p", "--xpitch", "--pitch"},
            required = true,
            description = "The x distance between centers from the template part to the first target part and between target parts")
    private Decimal6f xPitch = new Decimal6f("100");

    @CommandLine.Option(
            names = {"-c", "--xcount"},
            description = "The number of clones to make per line, the template itself counts as number 0 on the first line")
    private Integer xCount = Integer.MAX_VALUE;

    @CommandLine.Option(
            names = {"--ypitch"},
            description = "The y distance between centers between the template part and the first target part and between target parts")
    private Decimal6f yPitch = new Decimal6f("100");

    @Override
    public Integer call() throws Exception {
        LayoutCloner layoutCloner = new LayoutCloner(pcbCmd.getBoard());

        if (referenceOffsets.size() < 1) {
            return Fail.it("The list of reference offsets must contain at least one integer,\n" +
                    "like this: pcb -f thingy.kicad_pcb array-clone 100 200");
        }

        Layout layout = new ArrayLayout(referenceOffsets.getInts(), xPitch, xCount, yPitch);

        CloneResult cr = layoutCloner.cloneAndStore(layout);
        System.out.println(cr);

        return 0;
    }
}
