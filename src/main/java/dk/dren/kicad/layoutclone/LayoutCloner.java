package dk.dren.kicad.layoutclone;

import dk.dren.kicad.pcb.*;
import dk.dren.kicad.primitive.ModuleReference;
import dk.dren.kicad.primitive.Point;
import lombok.Getter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Getter
public class LayoutCloner {
    public static final String CMTS_USER = "Cmts.User";

    private final List<Module> templateModules;
    private final Board board;
    private final File pcbFile;
    private final Zone templateZone;
    private final File backupDir;
    private final File backupPcbFile;
    private final File reportFile;
    private final File reportBackupFile;

    public LayoutCloner(Board board) {
        this.board = board;
        this.pcbFile = board.getFile();

        LocalDateTime now = LocalDateTime.now();
        String timestamp = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-MM-SS"));
        backupDir = new File(pcbFile.getParentFile(), "backup");

        backupPcbFile = new File(backupDir, "backup-"+timestamp+"-"+pcbFile.getName());
        reportFile = new File(pcbFile.getParentFile(), pcbFile.getName()+".kumods.json");
        reportBackupFile = new File(backupDir, "backup-"+timestamp+"-"+ reportFile.getName());

        Optional<Zone> tz = board.zones().filter(z -> z.getLayer().equals(CMTS_USER)).findFirst();
        if (!tz.isPresent()) {
            throw new IllegalArgumentException("Could not find a zone on the " + CMTS_USER + " layer");
        }

        templateZone = tz.get();

        templateModules = board.modules().filter(m -> templateZone.getPolygon().contains(m)).collect(Collectors.toList());
    }

    public int removeGeneratedElementsAndStore() throws IOException {
        if (reportFile.isFile()) {
            int removedItems = removeGeneratedElementsFromClone(reportFile);
            try (Writer writer = new FileWriter(pcbFile)) {
                KicadPcbSerializer.write(board, writer);
            }
            reportFile.delete();
            return removedItems;
        } else {
            return -1;
        }
    }

    public CloneResult cloneAndStore(Layout layout) throws IOException {
        CloneResult res = new CloneResult();

        res.setBackupPcbFile(backupPcbFile.getName());
        res.setPcbFile(pcbFile.getName());
        res.setDirectory(pcbFile.getCanonicalFile().getParentFile());
        res.setReportFile(reportFile.getName());

        if (reportFile.isFile()) {
            res.setOldElementsRemoved(removeGeneratedElementsFromClone(reportFile));
        } else {
            res.setOldElementsRemoved(-1);
        }

        templateModules.forEach(templateModule -> {
            for (LayoutOffset offset : layout.getOffsets()) {
                ModuleReference referenceToMove = templateModule.getReference().add(offset.getReference());
                Module moduleToMove = board.getModuleWithReference(referenceToMove);
                if (moduleToMove == null) {
                    throw new IllegalArgumentException("Didn't find a module with reference "+referenceToMove+" needed because module "+templateModule.getReference()+" is in the template");
                }
                moduleToMove.getPosition().setXyr(templateModule.getPosition().getXyr().add(offset.getXyr()));
                moduleToMove.cloneLayerAndStyle(templateModule);
                res.addMovedModule(moduleToMove);
            }
        });


        // Clone all vias in the template zone, move them the offset and remove the net information
        board.vias()
                .filter(via -> templateZone.getPolygon().containsNode(via, Position.AT))
                .forEach(templateVia-> {

                    layout.getOffsets().forEach(offset -> {
                        RawNode via = KicadPcbSerializer.clone(templateVia);

                        Position pos = (Position) via.getProperty(Position.AT);
                        pos.setXyr(pos.getXyr().add(offset.getXyr()));

                        via.removeChild(via.getProperty("net"));

                        board.addChild(via);
                        res.addVia(via);
                    });
                });


        // Clone all the tracks in the template zone, move them the offset and remove the net information
        board.segments()
                .filter(segment -> templateZone.getPolygon().containsNode(segment, Position.START) || templateZone.getPolygon().containsNode(segment, Position.END))
                .forEach(templateSegment-> {

                    layout.getOffsets().forEach(offset -> {
                        RawNode segment = KicadPcbSerializer.clone(templateSegment);

                        Position start = (Position) segment.getProperty(Position.START);
                        start.setXyr(start.getXyr().add(offset.getXyr()));

                        Position end = (Position) segment.getProperty(Position.END);
                        end.setXyr(end.getXyr().add(offset.getXyr()));

                        segment.removeChild(segment.getProperty("net"));

                        board.addChild(segment);
                        res.addSegment(segment);
                    });
                });

        if (!backupDir.isDirectory() && !backupDir.mkdir()) {
            FileUtils.forceMkdir(backupDir);
        }
        FileUtils.copyFile(pcbFile, backupPcbFile);
        res.store(reportFile);
        FileUtils.copyFile(reportFile, reportBackupFile);

        try (Writer writer = new FileWriter(pcbFile)) {
            KicadPcbSerializer.write(board, writer);
        }

        return res;
    }

    private int removeGeneratedElementsFromClone(File reportFile) throws IOException {
        CloneResult old = CloneResult.load(reportFile);

        List<RawNode> goners = new ArrayList<>();

        Set<Point> positionsOfViasToNuke = new TreeSet<>(old.getViasCreated());
        board.vias()
                .filter(n -> positionsOfViasToNuke.contains(Position.ofVia(n)))
                .forEach(goners::add);

        Set<Line> positionsOfSegmentsToNuke = new TreeSet<>(old.getSegmentsCreated());
        board.segments()
                .filter(n -> positionsOfSegmentsToNuke.contains(Line.of(n)))
                .forEach(goners::add);

        goners.forEach(board::removeChild);

        return goners.size();
    }
}
